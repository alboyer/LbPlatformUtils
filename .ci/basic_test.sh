#!/usr/bin/env bash
set -e

# Print each command before it is ran
set -x

PY_VERS=$(python -c 'import sys; print(sys.version.split()[0])')

# some of the containers used for testing rely on LCG Python build in /usr/local
if [ $(which python) = /usr/local/bin/python -a -e /usr/local/lib/libpython2.7.so.1.0 ] ; then
    export LD_LIBRARY_PATH=/usr/local/lib${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
fi

rm -rf .cache/venv

case $PY_VERS in
    2.*)
        if ! which virtualenv >/dev/null 2>&1 ; then pip install virtualenv ; fi
        virtualenv .cache/venv
        ;;
    *)
        python -m venv .cache/venv
        ;;
esac

. .cache/venv/bin/activate

set -uo pipefail
IFS=$'\n\t'

pip install --upgrade pip

case $PY_VERS in
    2.7.*)
        pip install mock backports.tempfile
        ;;
esac

pip install pytest
pip install .

python --version

if which singularity >/dev/null 2>&1 ; then
    singularity --version
fi

pytest

lb-describe-platform --flags

lb-debug-platform --no-exit-with-error-count --help
lb-debug-platform --no-exit-with-error-count --debug
lb-debug-platform --no-exit-with-error-count --output=platform-info.json
